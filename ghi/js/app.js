function createCard(name, description, picture_url, location, start, end) {
    let startReform = new Date(start)
    let endReform = new Date(end)
    let date1 = startReform.toLocaleDateString()
    let date2 = endReform.toLocaleDateString()

    return `
    <div>
        <div class="card shadow mb-5 bg-body-tertiary rounded">
            <img src="${picture_url}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer bg-light border-dark">
                ${date1} - ${date2}
            </div>
        </div>
    </div>
    `;
}

function noConferences() {
    return `
    <div class="alert alert-primary" role="alert">
        It looks like there are no conferences in your area...
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col');
    console.log(columns);
    let colIndex = 0;
    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Something when response bad
            console.log('yeah')
            const column = document.querySelector('.col');
            column.innerHTML += noConferences();
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const conferenceDetails = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = details.conference.starts;
                    const endDate = details.conference.ends;
                    const conventionCenter = details.conference.location.name;
                    const html = createCard(title, conferenceDetails, pictureUrl, conventionCenter, startDate, endDate)
                    const column = columns[colIndex % 3]
                    column.innerHTML += html;
                    colIndex++;

                }
            }

        }
    } catch (e) {
        //Something when an error is raised
        console.error(e);
    }
    // const response = await fetch(url)
    // console.log(response);

    // const data = await response.json();
    // console.log(data);
});
