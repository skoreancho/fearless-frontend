window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const conferenceSelect = document.getElementById('conference');
    try {
        const response = await fetch(url);
        if (!response.ok) {
            // Some error
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const option = document.createElement('option');
                option.value = conference.id
                option.innerHTML = conference.name
                conferenceSelect.appendChild(option)
            }
        }
        const formTag = document.getElementById('create-presentation-form');
        console.log(formTag)
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            console.log(formData)
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json)
            // const presentationUrl = `http://localhost:8000/api/conferences/${json.conference}/`;
            // const fetchConfig = {
            //     method: "post",
            //     body: json,
            //     headers: {
            //         'Content-Type': 'application/json',
            //     },
            // };
            // const response = await fetch(conferenceUrl, fetchConfig);
            // if (response.ok) {
            //     formTag.reset();
            //     const newConference = await response.json();
            // }
        });
    } catch (e) {
        //         //Something when an error is raised
        // console.error(e);
    }
        //     }
        //     // const response = await fetch(url)
        //     // console.log(response);

        //     // const data = await response.json();
        //     // console.log(data);
});
