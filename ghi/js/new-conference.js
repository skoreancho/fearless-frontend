// window.addEventListener('DOMContentLoaded', async () => {
//     const url = http://localhost:8000/api/locations/;

// })
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const locationSelect = document.getElementById('location');
    try {
        const response = await fetch(url);
        if (!response.ok) {
            // Some error
        } else {
            const data = await response.json();
            // console.log(data)
            console.log(locationSelect)
            for (let location of data.locations) {
                const option = document.createElement('option');
                option.value = location.id
                option.innerHTML = location.name
                locationSelect.appendChild(option)
            }
        }
        const formTag = document.getElementById('create-conference-form');
        console.log(formTag)
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json)
            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
            }
        });
    } catch (e) {
        //         //Something when an error is raised
        // console.error(e);
    }
        //     }
        //     // const response = await fetch(url)
        //     // console.log(response);

        //     // const data = await response.json();
        //     // console.log(data);
});






//         if (!response.ok) {
//             // Something when response bad
//             // console.log('yeah')
//             // const column = document.querySelector('.col');
//             // column.innerHTML += noConferences();
//         } else {
//             const data = await response.json();
//             for (let location of data.states) {
//                 const option = document.createElement('option')
//                 option.value = stateInfo.abbreviation;
//                 option.innerHTML = stateInfo.name;
//                 stateSelect.appendChild(option);
//             }
//         }
//         const formTag = document.getElementById('create-location-form');
//         formTag.addEventListener('submit', async event => {
//             event.preventDefault();
//             const formData = new FormData(formTag);
//             const json = JSON.stringify(Object.fromEntries(formData));
//             const locationUrl = 'http://localhost:8000/api/locations/';
//             const fetchConfig = {
//                 method: "post",
//                 body: json,
//                 headers: {
//                     'Content-Type': 'application/json',
//                 },
//             };
//             const response = await fetch(locationUrl, fetchConfig);
//             if (response.ok) {
//                 formTag.reset();
//                 const newLocation = await response.json();
//             }
//         })
//     } catch (e) {
//         //Something when an error is raised
//         console.error(e);
//     }
//     // const response = await fetch(url)
//     // console.log(response);

//     // const data = await response.json();
//     // console.log(data);
// });
