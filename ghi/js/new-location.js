
// "We need to add an event listener for when the DOM loads."
// "Let's declare a variable that will hold the URL for the API that we just created."
// "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
// "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."

// function addState(state) {
//     return`
//     <option value="">${state}</option>
//     `;
// }
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
    const stateSelect = document.getElementById('state');
    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Something when response bad
            // console.log('yeah')
            // const column = document.querySelector('.col');
            // column.innerHTML += noConferences();
        } else {
            const data = await response.json();
            console.log(data)
            for (let stateInfo of data.states) {
                const option = document.createElement('option')
                option.value = stateInfo.abbreviation;
                option.innerHTML = stateInfo.name;
                stateSelect.appendChild(option);
            }
        }
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
            }
        })
    } catch (e) {
        //Something when an error is raised
        // console.error(e)
    }
    // const response = await fetch(url)
    // console.log(response);

    // const data = await response.json();
    // console.log(data);
});
