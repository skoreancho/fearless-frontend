import React, {useEffect, useState} from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [name, setNames] = useState('');
    const [starts, setStart] = useState('');
    const [ends, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setPresentations] = useState('');
    const [maxAttendees, setAttendees] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = event => {
        const value = event.target.value;
        setNames(value);
    }

    const handleStartChange = event => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEndChange = event => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescriptionChange = event => {
        const value = event.target.value;
        setDescription(value);
    }

    const handlePresentationsChange = event => {
        const value = event.target.value;
        setPresentations(value);
    }

    const handleAttendeesChange = event => {
        const value = event.target.value;
        setAttendees(value);
    }

    const handleLocationChange = event => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async event => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data)

        const url = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setNames('');
            setStart('');
            setEnd('');
            setDescription('');
            setPresentations('');
            setAttendees('');
            setLocation('');
        }
    };

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                <label htmlFor="Starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="Ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} required type="textarea" name="description" id="description" className="form-control" rows="5"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentationsChange} value={maxPresentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendeesChange} value={maxAttendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" name = "location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map( location => {
                    return (
                        <option key={location.href} value={location.id}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

}

export default ConferenceForm;
