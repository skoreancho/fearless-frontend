import React, {useEffect, useState} from 'react';

function PresentationForm () {
    const [presenterName, setNames] = useState('');

    const [companyName, setCompany] = useState('');

    const [presenterEmail, setEmail] = useState('');

    const [title, setTitle] = useState('');

    const [synopsis, setSynopsis] = useState('');

    const [status, setStatus] = useState('');

    const [conference, setConference] = useState('');

    const [conferences, setConferences] = useState([]);

    const handleNameChange = event => {
        const value = event.target.value;
        setNames(value);
    }

    const handleCompanyChange = event => {
        const value = event.target.value;
        setCompany(value);
    }

    const handleEmailChange = event => {
        const value = event.target.value;
        setEmail(value);
    }

    const handleTitleChange = event => {
        const value = event.target.value;
        setTitle(value);
    }

    const handleSynopsisChange = event => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const handleStatusChange = event => {
        const value = event.target.value;
        setStatus(value);
    }

    const handleConferenceChange = event => {
        const value = event.target.value;
        setConference(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.presenter_name = presenterName;
        data.company_name = companyName;
        data.presenter_email = presenterEmail;
        data.title = title;
        data.synopsis = synopsis;
        data.status = status;
        data.conference = conference;

        console.log(data);

        const presentationUrl = `http://localhost:8000${data.conference}presentations/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            setNames('');
            setCompany('');
            setEmail('');
            setTitle('');
            setSynopsis('');
            setStatus('');
            setConference('');
        }
    };



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setConferences(data.conferences)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input onChange={ handleNameChange } value={presenterName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={ handleCompanyChange } value={companyName} placeholder="Company" required type="text" name="company" id="company" className="form-control" />
                            <label htmlFor="company">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={ handleEmailChange } value={presenterEmail} placeholder="Email" required type="email" name="email" id="email" className="form-control" />
                            <label htmlFor="email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={ handleTitleChange } value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea onChange={ handleSynopsisChange } value={synopsis} placeholder="Synopsis" required type="textarea" name="synopsis" id="synopsis" className="form-control" rows="5" />
                        </div>
                        {/* <div className="form-floating mb-3">
                            <input onChange={ handleCitiesChange } value={cities} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                            <label htmlFor="city">Status</label>
                        </div> */}
                        <div className="mb-3">
                            <select onChange={ handleConferenceChange } value={conference} required name="state" id="state" className="form-select">
                                <option value="">Choose a conference</option>
                                {conferences.map( conference => {
                                    return (
                                        <option key={conference.href} value={conference.href}>
                                            {conference.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default PresentationForm;
